#include <bits/stdc++.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <conio.h>
#include <string>
#include <windows.h>

using namespace std;
int color2=1; 

//  mencegah user menginput huruf
int pilihan_error () {
    int number=0;
    int alpha=0;
    char pilih=0;

    do
    {
        pilih = getch(); //  baca input dari user
        alpha = pilih - '0';

        if (alpha >= 0 && alpha <= 9)
        {
            number *= 10;
            number += alpha;
            cout << alpha;
        }

    }while(pilih != 13); //  13 = Return

    return number;
    getch();

}
// Mengubah Warna
void setcolor( unsigned char color ) {
  SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), color );
}
//  Mengubah warna sampai 14
void setcolor2( unsigned char color ) {

	if (color <=14) {
		setcolor(color);
		
	}
 	else  {
	color2++;
 	setcolor(color2);
 	if (color2==15) {
 		color2=1;
	 }
 	}
	 
}


// Loading
void loading() {

   cout << "[";
   for (int i = 0; i < 15; i++) {
      Sleep(85);
      setcolor(i);
      cout << "x";
      setcolor(7);
   }
   cout << "]";
}

//  Swap array
void swap (string *a, string *b)
{
    string temp = *a;
    *a = *b;
    *b = temp;
}

// Print kelompok
void print_klmpk (string arr[],int jmlh_anggta, int jmlh_perklmpk)
{
    int i,j,h;
    h = jmlh_anggta % jmlh_perklmpk;

    j=0;
    int jumlah = sizeof(arr);
    int fix3 = 1;


    system("CLS");
    cout << "================================================================="<<endl;
    cout << "================ Pengacakan Nama untuk Kelompok ================="<<endl;
    cout << "==================== Dari Jumlah "<<jmlh_anggta<<" Anggota ===================== "<<endl;
    cout << "=========== Dengan Jumlah Anggota per kelompok: "<<jmlh_perklmpk<<" ===============" <<endl;
    cout << "======= Terdapat "<<h<<" Anggota Yang tersisa dari kelompok ===========\n"<<endl;
    setcolor2(1);
    cout << "Kelompok 1"<<endl;
    cout << "================================================================="<<endl;

   for(i=0;i<jmlh_anggta;i++){

		
        if(jmlh_perklmpk == j) { 
        h++;
        setcolor2(fix3+1);
      
		cout << "\nKelompok "<<fix3+1<<endl;
        cout << "================================================================="<<endl;
            j=0;
        	fix3++;
        }
        
        cout << "  \t\t\t    ["<<i+1<<"] "<<arr[i]<<endl;
        j++;
        if(jmlh_perklmpk == j) {
        	cout << "=================================================================\n"<<endl;
		}
    
    }
        setcolor2(fix3+1);
    setcolor (7);



}



// menghasilkan nama random
//  menggunakan algoritma Fisher Yates shuffle
void randomize (string arr[], int n)
{
    //  gunakan srand time supaYa
    //  tidak mendapat hasil Yang sama
    //  pada saat program dijalankan
    srand (time(NULL));

    //  mulai dari indeks terakhir dan tukar satu persatu
    //  tidak perlu menjalankan elemen pertama itulah kenapa i > 0
    for (int i = n - 1; i > 0; i--)
    {
        //  ambil angka random dari 0 sampai i
        int j = rand() % (i + 1);

        //  tukar arr[i] dengan 
        //  elemen di random index
        swap(&arr[i], &arr[j]);
    }
}

//  prosedur lempar dadu
void mengocok_dadu(int x, int arr[])
{
	int random,hasil;
    for(int i=0; i<x;i++){
  

    random = rand()% 6;
    cout << endl;
    cout << "=============================="<<endl;
    cout << "======= Lemparan ke-"<<i+1<<" ========"<<endl;
    cout << "========   [ENTER]  =========="<<endl;
    getch();//  tidak menampilkan
    cout <<"==========    ";setcolor(3);cout << random[arr]; setcolor(7); cout << "   ============"<<endl;
    cout << "=============================="<<endl;
	}

}

//  prosedur random pin
void random_pin(int x) {
    const char angka[] = "0123456789";
    int generate = sizeof(angka)-1; //  10

    srand(time(0));
    setcolor( 2 );
    cout << "==========================="<<endl;
    cout << "Digit PIN adalah "<<x<<endl;
    cout << "Ini adalah PIN Anda: ";
    for(int i = 0; i < x; i++) {
    cout << angka[rand() % generate]; //  Menghasilkan angka random 1-9 selama perulangan
    }
    cout << "\n==========================="<<endl;
       setcolor( 7 );

}

//  Prosedur random password
void membuat_password(int x)
{
    char acak;
    int random;

    system("CLS");

    char huruf[]= {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','Q','Y','Z'};
    int angka[]= {0,1,2,3,4,5,6,7,8,9};
    setcolor(2);
    cout << "==================================" << endl;
  if( x <= 4) {
        cout << endl;
        cout << "Maaf, minimal harus 5 digit"<<endl;
        cout << "Silahkan masukkan ulang digit";
        cout << endl;


    } else {
        cout << endl;
        cout << "Digit password "<<x<<endl;
        cout << "Ini adalah password Anda: ";
        acak = huruf[rand() % 25];// melakukan pengacakan dan menyimpan hasil acak di variabel acak
        cout << acak;
        strlwr(huruf); //  merubah karakter pada variabel huruf menjadi huruf kecil


        for(int i = 0; i < x-3; i++) {
        cout << huruf[rand() % 25]; //  25 adalah jumlah alfabet
        };

        for(int i = 0; i < x-4; i++) {
        cout << angka[rand() % 10]; //  25 adalah jumlah alfabet
        if ( x = 7) {x--;}
            };
        }
        cout << "\n==================================" << endl;
        setcolor(7);

}


//  prosedur pilih acak
void random_picker(string things, int size, string any[]) {

srand(time(0));
    setcolor( 2 );
    cout << "========================================================="<<endl;
    cout << "Hasil acak dari semua "<<things<<" adalah: ";
    cout << any[rand() % size]; //  size adalah jumlah benda Yang akan diacak
    cout << "\n========================================================="<<endl;
    setcolor( 7 );
}

//  random number generator
void rand_generator(int minimum, int maximum) {

	if (minimum>maximum) {
        srand((unsigned)time(0));
       int random = ((rand() % (minimum + 1 - maximum)) + maximum); //  menentukan batas random
       setcolor( 2 );
        cout << "\n========================================================="<<endl;
         cout << "Angka acak dari "<<minimum<<"-"<<maximum<<" Adalah: ";
         cout << "-"<<random; //  print random
         cout << "\n========================================================="<<endl;
         setcolor( 7 );
    }

    else {
   	srand((unsigned)time(0));
       int random = ((rand() % (maximum + 1 - minimum)) + minimum); //  menentukan batas random
       setcolor( 2 );
         cout << "\n========================================================="<<endl;
         cout << "Angka acak dari "<<minimum<<"-"<<maximum<<" Adalah: ";
         cout << random;
         cout << "\n========================================================="<<endl;
         setcolor( 7 );
    }


}

void tersedia(int i) {

	if (i== 2 ){
        cout << "\n\n>>=================<<"<<endl;
		cout << "    Terimakasih!   "<<endl;
        cout << ">>=================<<"<<endl;
            loading();
        }
        else {
            cout << "\n\n========================"<<endl;
            cout << "PILIHAN TIDAK TERSEDIA"<<endl;
            cout << "========================"<<endl;
            loading();
}
}

//  Prosedur random list
void random_list(string urutan[], int size, string nama) {
    system("CLS");
    setcolor(2);
	cout << "============================================="<<endl;
	cout << "Berikut adalah urutan acak dari "<<nama<<endl;
    cout << endl;
	for(int i =0; i<size; i++) {
		cout << "  ["<<i+1<<"] "<<urutan [i]<<endl; //  print array
	}
	cout << "\n============================================"<<endl;
	setcolor(7);
}


void menu_utama()
{

    int pilih;
    char jawab;
    menu_pilih:
        system("CLS");
        cout << endl;
        cout << "=================================================" << endl;
        cout << "     \t--Just a Random Things--\t"<<endl;
        cout << "     \tMau Pilih Yang mana nih\t" << endl;
        cout << "=================================================" << endl;
        cout << "\n1. Membuat Kelompok" << endl;
        cout << "2. Melempar Dadu" << endl;
        cout << "3. Membuat Pin" << endl;
        cout << "4. Membuat Password" << endl;
        cout << "5. Pilih Acak" << endl;
        cout << "6. Random Number Generator" << endl;
        cout << "7. Random List"<<endl;
        cout << "8. Keluar"<<endl;
        cout << endl;
        cout << "Masukkan Pilihan : ";
        cin >> pilih;
        cout << endl;
        switch (pilih)
        {
            mengulang:
            cout << "\n\nAnda ingin mencoba menu lain?  (y/n)";
            cin>>jawab;
            system("CLS");
            if( jawab == 'y'|| jawab =='Y'){
                goto menu_pilih;
            } else if (jawab == 'n' || jawab == 'N') {
            	cout << "=========================================="<<endl;
                cout << "Terimakasih Telah Menggunakan Aplikasi ini"<<endl;
                cout << "=========================================="<<endl;
                exit(0);
        
            } else {
                cout << "\t    Pilihan tidak ada"<<endl;
                cout << "\tSilahkan masukkan y atau n"<<endl;
                goto mengulang;
            }


        // Random Team case
        case 1: {
            string arr[100];
            int jmlh_anggta,jmlh_klmpk,jmllh_perklmpk;

            system("CLS");
            cout << endl;
            cout << "=================================================" << endl;
            cout << "     \tMembuat Kelompok\t" << endl;
            cout << "=================================================" << endl;
            cout << "masukkan Jumlah Seluruh Anggota : ";
            jmlh_anggta = pilihan_error(); //  input jumlah anggota

            cout <<  "\nmasukkan Jumlah Anggota per Kelompok : ";
            jmllh_perklmpk = pilihan_error(); //  input anggota per kelompok 
            cout << "\n=================================================="<<endl;
            cin.ignore();
        for(int i=0; i<jmlh_anggta;i++){
        	
                cout << "masukkan Nama ke-"<<i+1<<" : ";
                 getline (cin, arr[i]); //  getline supaYa bisa spasi
              
            }
            //  shuffle dan print kelompok
            randomize (arr, jmlh_anggta);
            print_klmpk(arr, jmlh_anggta,jmllh_perklmpk);
            int pilih_kelompok;

        // Pilihan
        do {
	        cout << endl;
	        cout << "\n*******************"<<endl;
            cout << "* Acak lagi?"<<endl;
            cout << "* 1. Ya"<<endl;
            cout << "* 2. Tidak"<<endl;
            cout << "* 3. Ubah Anggota";
            cout << "\n*******************"<<endl;
            cout << "* Masukkan Pilihan: ";
            pilih_kelompok= pilihan_error(); //  agar user tidak input huruf yang bisa mengakibatkan infinite loop

        if (pilih_kelompok==1) {
            cout << endl;
            loading ();
            system("cls");

            cout << endl;
    	    randomize (arr, jmlh_anggta);
            print_klmpk(arr, jmlh_anggta,jmllh_perklmpk);
	    }
	    else if (pilih_kelompok==3) {
            system("CLS");
            cout << endl;
            cout << "=================================================" << endl;
            cout << "     \tMembuat Kelompok\t" << endl;
            cout << "=================================================" << endl;
            cout << "masukkan Jumlah Seluruh Anggota : ";
            jmlh_anggta = pilihan_error();

            cout <<  "\nmasukkan Jumlah Anggota per Kelompok : ";
            jmllh_perklmpk = pilihan_error();
            cout << "\n=================================================="<<endl;

        for(int i=0; i<jmlh_anggta;i++){
        	
                cout << "masukkan Nama ke-"<<i+1<<" : ";
                 getline (cin, arr[i]);
            }

        //  shuffle & print
        randomize (arr, jmlh_anggta);
        print_klmpk(arr, jmlh_anggta,jmllh_perklmpk);
	}

	else if (pilih_kelompok==2) {
		tersedia(pilih_kelompok);
        goto mengulang;
	}
    else {
        tersedia(pilih_kelompok);
        goto mengulang;
    }
    }while (pilih_kelompok!=2);
    }
     goto mengulang;

    break; // End of random team case

    // Roll dice
    case 2: {
        system("CLS");
        int angka[]={1,2,3,4,5,6};
        int pilih_dadu, dadu;
        srand(time(0));
            cout << endl;
            cout << "========================================" << endl;
            cout << "     \t Program Mengocok Dadu\t" << endl;
            cout << "========================================" << endl;
            cout << endl;
            cout << "Ingin Mengocok Dadu berapa kali nih : ";
            dadu = pilihan_error();
	        mengocok_dadu(dadu,angka);

        do {
	        cout << endl;
	        cout << "\n*******************"<<endl;
            cout << "* Lempar lagi?"<<endl;
            cout << "* 1. Ya"<<endl;
            cout << "* 2. Tidak"<<endl;
            cout << "* 3. Ubah jumlah keluaran dadu";
            cout << "\n*******************"<<endl;
            cout << "* Masukkan Pilihan: ";
            pilih_dadu = pilihan_error();

        if (pilih_dadu==1) {
            cout << endl;
            loading ();
            system("cls");

            cout << endl;
    	    mengocok_dadu(dadu,angka);
	    }
        else if (pilih_dadu==3) {
            cout << "\nMasukkan jumlah keluaran dadu Yang baru: ";
            dadu = pilihan_error();
            system("cls");
            mengocok_dadu(dadu,angka);
        }
        else if (pilih_dadu == 2) {
            tersedia(pilih_dadu);
            goto mengulang;
        }
	    else {
		    tersedia(pilih_dadu);
            goto mengulang;
	    }
        }while (pilih_dadu!=2);
    }
        break;



        // PIN case
        case 3:
            system("cls");
            cout << "======================================================================"<<endl;
            cout << "      Random PIN akan menghasilkan beberapa digit pin secara acak     "<<endl;
            cout << "                   Digit PIN adalah jumlah angka                      "<<endl;
            cout << "======================================================================"<<endl;
            int angka_pin,pilih_pin;

            cout << "Masukkan digit PIN: ";
            angka_pin = pilihan_error();
            cout << endl;
            random_pin(angka_pin);

        do {
	        cout << endl;
	        cout << "\n*******************"<<endl;
            cout << "* Acak lagi?"<<endl;
            cout << "* 1. Ya"<<endl;
            cout << "* 2. Tidak"<<endl;
            cout << "* 3. Ubah digit PIN";
            cout << "\n*******************"<<endl;
            cout << "* Masukkan Pilihan: ";
            pilih_pin = pilihan_error();

        if (pilih_pin==1) {
            cout << endl;
            loading ();
            system("cls");

            cout << endl;
    	    random_pin(angka_pin);
	    }
        else if (pilih_pin==3) {
            cout << "\nMasukkan digit PIN Yang baru: ";
            angka_pin = pilihan_error();
            system("cls");
            random_pin(angka_pin);
        }
        else if (pilih_pin == 2) {
            tersedia(pilih_pin);
            goto mengulang;
        }
	    else {
		    tersedia(pilih_pin);
            goto mengulang;
	    }
        }while (pilih_pin!=2);
            break; // End of PIN case




        // Password case
        case 4:
            int n,pilih_pass,car;
            system("cls");
            char acak;
            srand(time(0));
            cout << endl;
            cout << "==================================" << endl;
            cout << "     \tMembuat Password\t" << endl;
            cout << "==================================" << endl;
            cout << endl;
             cout << "Mau ada berapa digit nih : ";
            n = pilihan_error();
            membuat_password(n);

        do {
	        cout << endl;
	        cout << "\n*******************"<<endl;
            cout << "* Acak lagi?"<<endl;
            cout << "* 1. Ya"<<endl;
            cout << "* 2. Tidak (Kembali)"<<endl;
            cout << "* 3. Ubah digit Password"<<endl;
            cout << "*******************"<<endl;
            cout << "* Masukkan Pilihan: ";

		pilih_pass = pilihan_error();
        if (pilih_pass==1) {
            cout << endl;
            loading ();
            system("cls");
    	    membuat_password(n);
	    }
        else if (pilih_pass==3) {
            cout << "\nMasukkan digit Password Yang baru: ";
            n = pilihan_error();
            membuat_password(n);
        }
        else if (pilih_pass == 2) {
            tersedia(pilih_pass);
            goto mengulang;
        }
	    else {
		    tersedia(pilih_pass);
		    goto mengulang;
	    }

        }while (pilih_pass !=2);

     break; // End of Password case


        // Random Picker case
        case 5 : {
            system("cls");
            cout << "======================================"<<endl;
            cout << "            RANDOM PICKER             "<<endl;
            cout << "     Memilih sesuatu secara acak      "<<endl;
            cout << "======================================"<<endl;
            string anything[100];
            string benda;
            cin.ignore();
            int pilih_benda, how_many;
            cout << "\n======================================"<<endl;
            cout << "Masukkan nama benda yang ingin diacak: ";
            getline (cin, benda);
            cout << "\nAda berapa "<<benda<<"?: ";
            how_many = pilihan_error();
            cout << "\n======================================"<<endl;

            for (int i=0;i<how_many;i++) {
            cout <<"\nMasukkan nama " <<benda<<" ke-"<<i+1<<endl;
            getline(cin, anything[i]);
            }

            random_picker(benda, how_many, anything);


        do {
	        cout << endl;
	        cout << "\n*******************"<<endl;
            cout << "* Acak lagi?"<<endl;
            cout << "* 1. Ya"<<endl;
            cout << "* 2. Tidak";
            cout << "\n*******************"<<endl;
            cout << "* Masukkan Pilihan: ";
            pilih_benda = pilihan_error();

        if (pilih_benda==1) {
            cout << endl;
            loading ();
            system("cls");
            random_picker(benda, how_many, anything);
            }
        else if (pilih_benda==2) {
            tersedia(pilih_benda);
            goto mengulang;
        }

	   	else {
		    tersedia(pilih_benda);
		    goto mengulang;
	    }



        }while (pilih_benda!=2);
        }

            break; // End of Random Picker

    // Random Number Generator
    case 6 : {

        int bil_min,bil_mx,pilih6;
            system("cls");
            cout << "=============================="<<endl;
            cout << "    Random Number Generator   "<<endl;
            cout << "=============================="<<endl;
            cout << "\nMasukkan angka minimum: ";
            bil_min = pilihan_error();;
            cout << "\nMasukkan angka maximum: ";
            bil_mx = pilihan_error();;


        rand_generator(bil_min, bil_mx);


        do {
	        cout << endl;
	        cout << "\n*******************"<<endl;
            cout << "* Acak lagi?"<<endl;
            cout << "* 1. Ya"<<endl;
            cout << "* 2. Tidak"<<endl;
            cout << "* 3. Ubah Angka";
            cout << "\n*******************"<<endl;
            cout << "* Masukkan Pilihan: ";
            pilih6 = pilihan_error();
        if (pilih6 ==1) {
            cout << endl;
            loading ();
            system("cls");

            rand_generator(bil_min, bil_mx);
        }
        else if (pilih6 ==3) {
            system("cls");
		    cout << "\n============================="<<endl;
            cout << "   Masukkan Angka Yang baru  "<<endl;
            cout << "============================="<<endl;
   		    cout << "\nMasukkan angka minimum: ";
   		    bil_min = pilihan_error();
   		    cout << "\nMasukkan angka maximum: ";
   		    bil_mx = pilihan_error();
            cout << endl;
   		    rand_generator(bil_min, bil_mx);
        }
	    else if (pilih6 == 2 ){
		   tersedia(pilih6);
            goto mengulang;
        }
        else {
            tersedia(pilih6);
            goto mengulang;
        }

        }while (pilih6!=2);
        }
            break; // End of random number generator case

    case 7: {
	
            string nama_urutan;
            int jumlah_urutan, pilih_7;
            string list[100];
            cin.ignore();
            system ("cls");
            cout << "=============================================="<<endl;
            cout << "                 RANDOM LIST                  "<<endl;
            cout << "      Membuat urutan sesuatu secara acak      "<<endl;
            cout << "=============================================="<<endl;
            cout << endl;
            cout << "======================================="<<endl;
            cout << "list-nya mau dinamai apa? : ";
            getline (cin, nama_urutan);
            cout << endl;
            cout << "Ada berapa jumlah seluruh "<<nama_urutan<<": ";
            jumlah_urutan = pilihan_error();
            cout << "\n======================================="<<endl;
    
        for (int i=0; i<jumlah_urutan; i++) {
            cout <<"\nMasukkan nama " <<nama_urutan<<" ke-"<<i+1<<endl;
            getline(cin, list[i]);
            }
            cout << "===================================="<<endl;
            loading ();
            // shuffle and generate
            randomize (list, jumlah_urutan);
            random_list(list, jumlah_urutan, nama_urutan);
    
         do {
	        cout << endl;
	        cout << "\n*******************"<<endl;
            cout << "* Acak lagi?"<<endl;
            cout << "* 1. Ya"<<endl;
            cout << "* 2. Tidak";
            cout << "\n*******************"<<endl;
            cout << "* Masukkan Pilihan: ";
            pilih_7 = pilihan_error();

        if (pilih_7==1) {
            cout << endl;
            loading ();
            system("cls");
            
            randomize (list, jumlah_urutan);
    		random_list(list, jumlah_urutan, nama_urutan);
            }
        else if (pilih_7==2) {
            tersedia(pilih_7);
            goto mengulang;
        }

	   	else {
		    tersedia(pilih_7);
		    goto mengulang;
	    }

        }while (pilih_7!=2);
	}	
    
    break;
    
     case 8: 
            loading();
        	cout << "\n============================================================"<<endl;
            cout << "Terimakasih telah menggunakan program ini, have a nice day!"<<endl;
            cout << "============================================================"<<endl;
            break;
    
    default :
    	system("cls");

    		setcolor(2);
            cout << "\t\t\t\t\t============================="<<endl;
        	cout << "\t\t\t\t\tPilihan tidak tersedia!\n\t\t\t\t\tProgram otomatis keluar"<<endl;
            cout << "\t\t\t\t\t============================="<<endl;
            cout << "\t\t\t\t\t";
            loading();

        }

}


//  main
int main()
{
    menu_utama();

    return 0;
}
